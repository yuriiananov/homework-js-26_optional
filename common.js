"use strict";

document.addEventListener('DOMContentLoaded', function() {
  const prevBtn = document.querySelector('.prev-btn');
  const nextBtn = document.querySelector('.next-btn');
  const images = document.querySelectorAll('.slides img');

  let currentIndex = 0;

  images[currentIndex].classList.add('active');

  function showSlide(index) {
    images.forEach(function(image) {
      image.classList.remove('active');
    });
    images[index].classList.add('active');
  }

  prevBtn.addEventListener('click', function() {
      currentIndex = (currentIndex - 1 + images.length) % images.length;
      showSlide(currentIndex);
  });

  nextBtn.addEventListener('click', function() {
      currentIndex = (currentIndex + 1) % images.length;
      showSlide(currentIndex);
  });
});

